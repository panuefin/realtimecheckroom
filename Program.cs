﻿using LiteDB;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RealTimeCheckRoom
{
    class Program
    {

        public static config _config = new config();
        static async Task Main(string[] args)
        {

            try
            {
                string jsonPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config.json");

                Console.WriteLine("get config.json");
                

                
                List<cls_listServer> ro = new List<cls_listServer>();
                List<cls_webrtccheck> ret = new List<cls_webrtccheck>();

                if (System.IO.File.Exists(jsonPath))
                {
                    using (StreamReader r = new StreamReader(jsonPath))
                    {
                        string json = r.ReadToEnd();

                        Console.WriteLine(json);

                        _config = JsonConvert.DeserializeObject<config>(json);
                        ro = _config.server;
                    }


                }

                Console.WriteLine("get data webrtc");

                foreach (cls_listServer ser in ro)
                {
                    HttpClientHandler clientHandler = new HttpClientHandler(); 
                    clientHandler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;

                    using (var client = new HttpClient(clientHandler))
                    {

                        client.BaseAddress = new Uri(ser.url + "v1/rooms");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("MAuth", CreateMAuth(ser));



                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                        try
                        {
                            var httpRequestMessage = new HttpRequestMessage
                            {
                                RequestUri = new Uri(ser.url + "v1/rooms"),
                                Method = HttpMethod.Get
                            };

                            HttpResponseMessage response = await client.SendAsync(httpRequestMessage);


                            Console.WriteLine("getdata " + ser.name + "(" + ser.url + ")" );

                            string strResult = await response.Content.ReadAsStringAsync();
                            //var obj = JObject.Parse(strResult);
                            if (response.IsSuccessStatusCode)
                            {

                                JArray objdata = JArray.Parse(strResult);

                                int countdata = objdata.Count;

                                if(ser.check_stream)
                                {
                                    countdata = await CheckStreamsAsync(ser, objdata);
                                }

                                Console.WriteLine(ser.name + "(" + ser.url + ") Count :" + countdata);

                                var web = new cls_webrtccheck();
                                web.name = ser.name;
                                web.url = ser.url;
                                web.count = countdata + "";
                                web.status = Math.Round(((countdata / Double.Parse(ser.limit))*100)) + "";
                                ret.Add(web);

                                if ((Double.Parse(web.status) > _config.limit_alert) || countdata >= Double.Parse(ser.limit))
                                {
                                    string subject = "[" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "] : อีเมลแจ้งอัตโนมัติ " + ser.name + "(" + ser.url + ") " + "มีการใช้งานเกิน " + _config.limit_alert + "%";
                                    string body = "<div>Timestamp : " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US")) + "</div><br>Server : " + ser.name + "(" + ser.url + ") Rooms : " + countdata + " Limit : " + ser.limit + " (" + web.status + "%)";
                                    if(countdata >= Int32.Parse(ser.limit))
                                    {
                                        subject = "[" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "] : อีเมลแจ้งอัตโนมัติ " + ser.name + "(" + ser.url + ") " + "มีการใช้งานเต็มแล้ว";
                                    }
                                    SendMail(subject, body);
                                    Console.WriteLine(body);

                                }

                                //int update = ExecuteWrite("UPDATE webrtc_check set count = '" + countdata + "',timestamp=(datetime('now','localtime')) where name = '" + ser.name + "'");

                            }
                            else
                            {
                                var web = new cls_webrtccheck();
                                web.name = ser.name;
                                web.url = ser.url;
                                web.count = "0";
                                web.status = "0";
                                web.error = response.Content.ToString();
                                ret.Add(web);
                            }
                        }
                        catch (Exception ex)
                        {
                            var web = new cls_webrtccheck();
                            web.name = ser.name;
                            web.url = ser.url;
                            web.count = "0";
                            web.status = "0";
                            web.error = ex.Message;
                            ret.Add(web);
                            SendMail("[" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "] : อีเมลแจ้งอัตโนมัติ " + ser.name + "(" + ser.url + ") " + "พบการทำงานผิดปรกติ", "<div>Timestamp : " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US")) + "</div><br><div><p style=\"color: red\">Error : " + ex.Message + "</p></div>");
                            Console.WriteLine("Error : " + ex.Message);
                        }

                        
                    }
                }


                Console.WriteLine("send data to api");

                

                foreach(string url in _config.url)
                {
                    HttpClientHandler clientHandler2 = new HttpClientHandler();
                    clientHandler2.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;


                    using (var client2 = new HttpClient(clientHandler2))
                    {
                        
                        client2.BaseAddress = new Uri(url + "UpdateGetAllRooms");
                        client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));



                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                        var json = JsonConvert.SerializeObject(ret);

                        var httpRequestMessage2 = new HttpRequestMessage
                        {
                            RequestUri = new Uri(url + "UpdateGetAllRooms"),
                            Method = HttpMethod.Post,
                            Content = new StringContent(json, Encoding.UTF8, "application/json")
                        };

                        HttpResponseMessage response2 = await client2.SendAsync(httpRequestMessage2);


                        string strResult2 = await response2.Content.ReadAsStringAsync();
                        //var obj = JObject.Parse(strResult);
                        if (response2.IsSuccessStatusCode)
                        {
                            Console.WriteLine(strResult2);
                        }
                        else
                        {
                            Console.WriteLine(strResult2);
                        }
                    }
                }

                

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
        }

        public static async Task<int> CheckStreamsAsync(cls_listServer server, JArray data)
        {
            int ret = 0;
            try
            {
                for(int i = 0;i < data.Count;i++)
                {

                    HttpClientHandler clientHandler = new HttpClientHandler();
                    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                    using (var client = new HttpClient(clientHandler))
                    {


                        client.BaseAddress = new Uri(server.url + "v1.1/rooms/" + data[i]["id"].ToString() + "/streams");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("MAuth", CreateMAuth(server));



                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;



                        var httpRequestMessage = new HttpRequestMessage
                        {
                            RequestUri = new Uri(server.url + "v1.1/rooms/" + data[i]["id"].ToString() + "/streams"),
                            Method = HttpMethod.Get
                        };

                        HttpResponseMessage response = await client.SendAsync(httpRequestMessage);


                        string strResult = await response.Content.ReadAsStringAsync();
                        JArray obj = JArray.Parse(strResult);
                        if (response.IsSuccessStatusCode)
                        {
                            if(obj.Count > 1)
                            {
                                ret++;
                            }
                        }
                        else
                        {
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
            return ret;
        }

        /*public static void CreateDatabaseAndTable()
        {
            SQLiteConnection con;
            SQLiteCommand cmd;
            SQLiteDataReader dr;
            string sql = "";

            string jsonPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config.json");

            config _config = new config();
            List<cls_listServer> ro = new List<cls_listServer>();

            if (System.IO.File.Exists(jsonPath))
            {
                using (StreamReader r = new StreamReader(jsonPath))
                {
                    string json = r.ReadToEnd();
                    _config = JsonConvert.DeserializeObject<config>(json);
                    ro = _config.server;
                }
            }

            if (!File.Exists("databasecheck.db"))
            {

                Console.WriteLine("create database");

                string insert = "";
                int i = 0;
                foreach (cls_listServer ser in ro)
                {
                    i++;
                    insert += "INSERT INTO webrtc_check VALUES (" + i + ",'" + ser.url + "',0,'" + ser.name + "',(datetime('now','localtime')));";
                }

                SQLiteConnection.CreateFile("databasecheck.db");

                sql = @"BEGIN TRANSACTION;
                CREATE TABLE IF NOT EXISTS webrtc_check (
	                id	INTEGER NOT NULL ,
	                url	TEXT NOT NULL,
	                count	INTEGER NOT NULL DEFAULT 0,
	                name	TEXT NOT NULL,
                    timestamp datetime default current_timestamp,
	                PRIMARY KEY(id)
                );" + insert + "COMMIT;";
                con = new SQLiteConnection("Data Source=databasecheck.db;Version=3;");
                con.Open();
                cmd = new SQLiteCommand(sql, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            else
            {
                string count_table = " SELECT name FROM webrtc_check";

                con = new SQLiteConnection("Data Source=databasecheck.db;Version=3;");
                con.Open();

                Dictionary<string, string> dict = new Dictionary<string, string>();

                cmd = new SQLiteCommand(count_table, con);
                SQLiteDataReader r = cmd.ExecuteReader();

                int ctable = 0;

                while (r.Read())
                {
                    dict.Add(Convert.ToString(r["name"]), Convert.ToString(r["name"]));
                    ctable++;
                }

                con.Close();

                count_table = " SELECT max(id) as max FROM webrtc_check";

                con = new SQLiteConnection("Data Source=databasecheck.db;Version=3;");
                con.Open();

                cmd = new SQLiteCommand(count_table, con);
                r = cmd.ExecuteReader();

                int max = 0;

                while (r.Read())
                {
                    max = Int32.Parse(Convert.ToString(r["max"]));
                }
                con.Close();

                if (ro.Count != ctable)
                {
                    string insert_table = "";
                    foreach (cls_listServer ser in ro)
                    {
                        if (!dict.ContainsKey(ser.name))
                        {
                            insert_table += "INSERT INTO webrtc_check VALUES ('" + (max+1) + "','" + ser.url + "',0,'" + ser.name + "',(datetime('now','localtime')));";
                        }
                    }

                    if (insert_table != "")
                    {
                        Console.WriteLine("INSERT table");

                        SQLiteConnection con2 = new SQLiteConnection("Data Source=databasecheck.db;Version=3;");
                        con2.Open();
                        SQLiteCommand cmd2 = new SQLiteCommand(insert_table, con2);
                        cmd2.ExecuteNonQuery();
                        con2.Close();
                    }
                    else
                    {
                        Console.WriteLine("data not update");
                    }

                }
                
            }
        }*/

         private static void SendMail(string subject,string body)
        {
            try
            {
                MailMessage mm = new MailMessage();
                SmtpClient smtp = new SmtpClient();


                mm.From = new MailAddress(_config.mailserver.from_address, "DisplayName", System.Text.Encoding.UTF8);
                mm.To.Add(new MailAddress(_config.mailserver.to_address));
                mm.Subject = subject;
                mm.Body = body;

                mm.IsBodyHtml = true;
                //smtp.Host = "smtp.gmail.com";
                smtp.Host = _config.mailserver.server_ip;

                smtp.EnableSsl = false;
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = _config.mailserver.username;//gmail user name
                NetworkCred.Password = _config.mailserver.password;// password
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = NetworkCred;
                smtp.Port = 25; //Gmail port for e-mail 465 or 587
                smtp.Send(mm);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
            

        }

        /*private static int ExecuteWrite(string query)
        {
            int numberOfRowsAffected;

            CreateDatabaseAndTable();

            //setup the connection to the database
            //using (var con = new SQLiteConnection("Data Source=" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "databasecheck.db") + ";Version=3;New=False;Compress=True;"))
            using (var con = new SQLiteConnection("Data Source=databasecheck.db;Version=3;New=False;Compress=True;"))
            {
                con.Open();

                //open a new command
                using (var cmd = new SQLiteCommand(query, con))
                {

                    //execute the query and get the number of row affected
                    numberOfRowsAffected = cmd.ExecuteNonQuery();
                }

                return numberOfRowsAffected;
            }
        }*/


        private static bool ServerCertificateCustomValidation(HttpRequestMessage requestMessage, X509Certificate2 certificate, X509Chain chain, SslPolicyErrors sslErrors)
        {
            // It is possible inpect the certificate provided by server
            Console.WriteLine($"Requested URI: {requestMessage.RequestUri}");
            Console.WriteLine($"Effective date: {certificate.GetEffectiveDateString()}");
            Console.WriteLine($"Exp date: {certificate.GetExpirationDateString()}");
            Console.WriteLine($"Issuer: {certificate.Issuer}");
            Console.WriteLine($"Subject: {certificate.Subject}");

            // Based on the custom logic it is possible to decide whether the client considers certificate valid or not
            Console.WriteLine($"Errors: {sslErrors}");
            return sslErrors == SslPolicyErrors.None;
        }

        private static string DataHmacSHA256(string key, string data)
        {
            string hash;
            ASCIIEncoding encoder = new ASCIIEncoding();
            Byte[] code = encoder.GetBytes(key);
            using (HMACSHA256 hmac = new HMACSHA256(code))
            {
                Byte[] hmBytes = hmac.ComputeHash(encoder.GetBytes(data));
                hash = ToHexString(hmBytes);
            }
            return Base64Encode(hash);
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static string ToHexString(byte[] array)
        {
            StringBuilder hex = new StringBuilder(array.Length * 2);
            foreach (byte b in array)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }
        private static string CreateMAuth(cls_listServer server)
        {
            Random rnd = new Random();
            var cnonce = rnd.Next(100000);
            //cnonce = 36553;

            DateTime foo = DateTime.UtcNow;
            long unixTime = ((DateTimeOffset)foo).ToUnixTimeMilliseconds();
            //unixTime = 1605602290416;

            string ServiceId = server.ServiceId;
            string ServiceKey = server.ServiceKey;
            string signature = DataHmacSHA256(ServiceKey, unixTime + "," + cnonce);


            return "realm=http://marte3.dit.upm.es,mauth_signature_method=HMAC_SHA256,mauth_serviceid=" + ServiceId + ",mauth_cnonce=" + cnonce + ",mauth_timestamp=" + unixTime + ",mauth_signature=" + signature;
            //return "realm=http://marte3.dit.upm.es,mauth_signature_method=HMAC_SHA256,mauth_username=" + username + ",mauth_serviceid=" + ServiceId + ",mauth_cnonce=" + cnonce + ",mauth_timestamp=" + unixTime + ",mauth_signature=" + signature;
        }
    }


    public class config
    {
        public List<string> url { get; set; }
        public List<cls_listServer> server { get; set; }
        public mailserver mailserver { get; set; }
        public int limit_alert { get; set; } = 0;
    }

    public class mailserver
    {
        public string server_ip { get; set; }
        public string port { get; set; }
        public string from_address { get; set; }
        public string to_address { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class cls_listServer
    {
        public string ServiceId { get; set; }
        public string ServiceKey { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string limit { get; set; }
        public bool check_stream { get; set; } = false;
    }

    public class cls_webrtccheck
    {
        public string id { get; set; }
        public string url { get; set; }
        public string count { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string ServiceId { get; set; }
        public string ServiceKey { get; set; }
        public string error { get; set; }
    }
}
